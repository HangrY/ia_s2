//
// Created by quent on 4/23/2020.
//

#ifndef IA_S2_PIZZASTATE_H
#define IA_S2_PIZZASTATE_H

/**
 * The state of the pizza
 */
struct PizzaState {
    bool ingredients;
    bool prepared;
    bool cooked;
};


#endif //IA_S2_PIZZASTATE_H
