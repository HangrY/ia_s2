//
// Created by quent on 4/23/2020.
//

#ifndef IA_S2_GATHERNODE_H
#define IA_S2_GATHERNODE_H

#include "Node.h"
#include "PizzaState.h"

#include <iostream>

/**
 * Node to gather the pizza ingredients
 */
class GatherNode : public Node<PizzaState> {
public:
    /**
     * Gather pizza ingredients
     * @param [out] state ingredients set to true
     * @return SUCCESS
     */
    ActionState execute(PizzaState &state, Node<PizzaState> *) final {
        state.ingredients = true;
        std::cout << "Gathering ingredients." << std::endl;
        return ActionState::SUCCESS;
    }

    Node<PizzaState> * copy() const final {
        return new GatherNode();
    }
};


#endif //IA_S2_GATHERNODE_H
