//
// Created by quent on 4/22/2020.
//

#ifndef IA_S2_NODE_H
#define IA_S2_NODE_H

#include "ActionState.h"

/**
 * The Node class is a template abstract class.
 * Classes derived from it will create the behavior tree
 *
 * @tparam State type used to represent a specific state
 */
template<typename State>
class Node {
public:
    /**
     * The execute function is a pure virtual function used to create actions nodes in the behavior tree.
     *
     * @param [in] state const reference to the state, if you need to modify it make a local copy.
     * This behavior is necessary to avoid the cost of useless copy when the state will not be modified
     * @param [out] current_node when a node returns RUNNING, this parameter is set to to node to execute again
     * @return SUCCESS if the action was executed for the passed state, RUNNING if the action is executing, FAILURE otherwise
     */
    virtual ActionState execute(State &state, Node<State> *current_node) = 0;

    /**
     * Each derived has to be able to copy itself in case of manual memory management
     * @return a pointer to a copy of the node
     */
    virtual Node<State> *copy() const = 0;
};


#endif //IA_S2_NODE_H
