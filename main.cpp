#include "SelectorNode.h"
#include "SequenceNode.h"
#include "CookNode.h"
#include "PrepareNode.h"
#include "GatherNode.h"
#include "Runner.h"

#include "json.hpp"

#include <iostream>
#include <fstream>

using namespace nlohmann;

Node<PizzaState> *createBehaviorTreeFromJson(const json &json_tree) {
    if (!json_tree.is_object())
        throw std::runtime_error("wrong json format");

    Node<PizzaState> *n;

    std::string type = json_tree["type"];
    if (type == "selector") {
        auto sn = new SelectorNode<PizzaState>();
        for (const json &nn : json_tree["children"])
            sn->addChild(createBehaviorTreeFromJson(nn));
        n = sn;

    } else if (type == "sequence") {
        auto sn = new SequenceNode<PizzaState>();
        for (const json &nn : json_tree["children"])
            sn->addChild(createBehaviorTreeFromJson(nn));
        n = sn;
    } else if (type == "cook") {
        n = new CookNode();
    } else if (type == "prepare") {
        n = new PrepareNode();
    } else if (type == "gather") {
        n = new GatherNode();
    } else
        throw std::runtime_error("wrong node type '" + type + "'");

    return n;
}

int main() {
    json json_tree;
    std::ifstream f("../tree.json");
    f >> json_tree;

    //std::cout << json_tree << std::endl;

    PizzaState state{false, false, false};

    auto n = createBehaviorTreeFromJson(json_tree);

    Runner<PizzaState> runner(state, n);

    runner.run();

    free(n);

    return 0;
}
