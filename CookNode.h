//
// Created by quent on 4/22/2020.
//

#ifndef IA_S2_COOKNODE_H
#define IA_S2_COOKNODE_H

#include "Node.h"
#include "PizzaState.h"
#include "ActionState.h"

#include <iostream>

/**
 * Leaf node to cook the pizza
 */
class CookNode : public Node<PizzaState> {
public:
    /**
     * Cooks the pizza if it is prepared (it takes 3 iterations to cook it)
     * @param [out] state only modified if the pizza is cooked (returns SUCCESS)
     * @return FAILURE if the pizza is not prepared, RUNNING if the pizza is cooking, SUCCESS when it is cooked
     */
    ActionState execute(PizzaState &state, Node<PizzaState> *) final {
        if (!state.prepared)
            return ActionState::FAILURE;
        if (_cook_time-- == 0) {
            state.cooked = true;
            state.prepared = false;
            std::cout << "Pizza is cooked! :D" << std::endl;
            return ActionState::SUCCESS;
        }
        std::cout << "Pizza is cooking." << std::endl;
        return ActionState::RUNNING;
    }

    Node<PizzaState> *copy() const final {
        return new CookNode;
    }

private:
    int _cook_time = 3;
};


#endif //IA_S2_COOKNODE_H
