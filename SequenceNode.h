//
// Created by Bastien on 22/04/2020.
//

#ifndef IA_S2_SEQUENCENODE_H
#define IA_S2_SEQUENCENODE_H


#include "Node.h"
#include <algorithm>
#include <vector>

/**
 * The SequenceNode class derived from Node is a node are used to find and execute the first child that does not success.
 *
 * @tparam State type used to represent a specific state
 */
template<typename State>
class SequenceNode : public Node<State> {
public:
    SequenceNode() = default;

    SequenceNode(const SequenceNode &other) {
        _children.reserve(other._children.size());
        for (auto c : other._children) {
            _children.push_back(c.copy());
        }
    }

    SequenceNode(SequenceNode &&other) noexcept {
        swap(other, *this);
    }

    SequenceNode &operator=(SequenceNode other) {
        swap(other, *this);
        return *this;
    }

    SequenceNode &operator=(SequenceNode &&other) noexcept {
        swap(other, *this);
        return *this;
    }

    ~SequenceNode() {
        for (auto c : _children)
            delete c;
    }

    /**
     * Executes the next possible node with a left to right priority.
     * will return immediately with a status code of failure when one of its children returns failure.
     *
     * @param [out] state passed to the children nodes
     * @return FAILURE or RUNNING when one of its children returns FAILURE or RUNNING, SUCCESS otherwise
     */
    ActionState execute(State &state, Node<State> *curr_node) override;

    void swap(SequenceNode &other) {
        using std::swap;
        swap(_children, other._children);
    }

    void addChild(Node<State> *n) {
        _children.push_back(n);
    }

    virtual Node<State> *copy() const {
        auto *cpy = new SequenceNode<State>();

        for (auto c : _children) {
            cpy->_children.push_back(c->copy());
        }

        return cpy;
    }

private:
    std::vector<Node<State> *> _children;
    std::size_t _curr_index = 0;
};

template<typename S>
void swap(SequenceNode<S> &a, SequenceNode<S> &b) {
    a.swap(b);
}

template<typename State>
ActionState SequenceNode<State>::execute(State &state, Node<State> *curr_node) {
    for (std::size_t i = _curr_index; i < _children.size(); ++i) {
        ActionState as = _children[i]->execute(state, curr_node);
        if (as == ActionState::FAILURE) return ActionState::FAILURE;
        if (as == ActionState::RUNNING) {
            _curr_index = i;
            curr_node = this;
            return ActionState::RUNNING;
        }
    }
    return ActionState::SUCCESS;
}

#endif //IA_S2_SEQUENCENODE_H
