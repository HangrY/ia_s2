//
// Created by quent on 4/23/2020.
//

#ifndef IA_S2_ACTIONSTATE_H
#define IA_S2_ACTIONSTATE_H


enum class ActionState {
    SUCCESS,
    FAILURE,
    RUNNING
};


#endif //IA_S2_ACTIONSTATE_H
