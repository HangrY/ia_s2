//
// Created by quent on 4/23/2020.
//

#ifndef IA_S2_PREPARENODE_H
#define IA_S2_PREPARENODE_H

#include "Node.h"
#include "PizzaState.h"

#include <iostream>

/**
 * Node to prepare the pizza
 */
class PrepareNode : public Node<PizzaState> {
public:
    /**
     * Prepares the pizza if the ingredients are ready
     * @param [out] state only modified if the pizza is prepared
     * @return SUCCESS is the pizza is prepared, FAILURE otherwise
     */
    ActionState execute(PizzaState &state, Node<PizzaState> *) final {
        if (!state.ingredients)
            return ActionState::FAILURE;
        state.ingredients = false;
        state.prepared = true;
        std::cout << "Preparing pizza." << std::endl;
        return ActionState::SUCCESS;
    }

    Node<PizzaState> *copy() const final {
        return new PrepareNode;
    }
};


#endif //IA_S2_PREPARENODE_H
