//
// Created by quent on 4/23/2020.
//

#ifndef IA_S2_RUNNER_H
#define IA_S2_RUNNER_H

#include "Node.h"
#include "ActionState.h"

template<typename State>
class Runner {
public:
    Runner(const State &state, Node<State> *tree) : _state(state), _tree(tree) {}

    ActionState run();

private:
    State _state;
    Node<State> *_tree;
};

template<typename State>
ActionState Runner<State>::run() {
    ActionState as;
    do {
        as = _tree->execute(_state, _tree);
    } while (as == ActionState::RUNNING);
    return as;
}

#endif //IA_S2_RUNNER_H
