Ce projet ce build avec cmake avec une toolchain MingGW:

cmake -DCMAKE_BUILD_TYPE=Release -G "CodeBlocks - MinGW Makefiles" "path_to_this_directory"

pour build l'executable:

cmake --build "path_to_directory/cmake-build-release" --target all

Sinon, ouvrir le projet avec CLion


Pour tester:
Modifier le behavior tree dans le fichier tree.json
Modifier l'état de départ dans le main.cpp l'instantiation de la classe PizzaState avec des valeurs différentes



Idée du projet:
Nous avons créer ce projet en imaginant que nous développions une librairie.
Une classe abstraite template Node permet la création d'un tree de classe dérivé de Node.
Une classe runner doit être utilisé en lui passant le tree et l'état de départ.

Un utilisateur de cette librairie pourra et devra creer lui-même ses classes d'actions (feuilles de l'arbre) qui devront hériter de Node.
Il pourra aussi créer d'autre type de node, imaginons un selector différent ou   