//
// Created by quent on 4/22/2020.
//

#ifndef IA_S2_SELECTORNODE_H
#define IA_S2_SELECTORNODE_H

#include "Node.h"

#include <algorithm>
#include <vector>

/**
 * The SelectorNode class derived from Node is a node are used to find and execute the first child that does not fail.
 *
 * @tparam State type used to represent a specific state
 */
template<typename State>
class SelectorNode : public Node<State> {
public:
    SelectorNode() = default;

    SelectorNode(const SelectorNode &other) {
        _children.reserve(other._children.size());
        for (auto c : other._children) {
            _children.push_back(c.copy());
        }
    }

    SelectorNode(SelectorNode &&other) noexcept {
        swap(other, *this);
    }

    SelectorNode &operator=(SelectorNode other) {
        swap(other, *this);
        return *this;
    }

    SelectorNode &operator=(SelectorNode &&other) noexcept {
        swap(other, *this);
        return *this;
    }

    ~SelectorNode() {
        for (auto c : _children)
            delete c;
    }

    /**
     * Executes the next possible node with a left to right priority.
     * will return immediately with a status code of success when one of its children returns success.
     *
     * @param [out] state passed to the children nodes
     * @return SUCCESS or RUNNING when one of its children returns success or RUNNING, FAILURE otherwise
     */
    ActionState execute(State &state, Node<State> *curr_node) override;

    void swap(SelectorNode &other) {
        using std::swap;
        swap(_children, other._children);
    }

    void addChild(Node<State> *n) {
        _children.push_back(n);
    }

    Node<State> *copy() const override {
        auto *copy = new SelectorNode<State>();

        for (auto c : _children) {
            copy->_children.push_back(c->copy());
        }

        return copy;
    }

private:
    std::vector<Node<State> *> _children;
    std::size_t _curr_index = 0;
};

template<typename S>
void swap(SelectorNode<S> &a, SelectorNode<S> &b) {
    a.swap(b);
}

template<typename State>
ActionState SelectorNode<State>::execute(State &state, Node<State> *curr_node) {
    for (std::size_t i = _curr_index; i < _children.size(); ++i) {
        ActionState as = _children[i]->execute(state, curr_node);
        if (as == ActionState::SUCCESS) return ActionState::SUCCESS;
        if (as == ActionState::RUNNING) {
            curr_node = this;
            _curr_index = i;
            return ActionState::RUNNING;
        }
    }
    return ActionState::FAILURE;
}

#endif //IA_S2_SELECTORNODE_H
